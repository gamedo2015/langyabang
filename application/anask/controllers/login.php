<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends My_Controller {

	/**
	 * Index Page for this controller.
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model('userm');
    $this->load->helper('captcha');

	}

	public function index()
	{
    //验证码
    $cap = $this->captcha();
    //验证是否已经登陆
    if($this->session->userdata('name'))
    {
      $this->smarty->display('index.html');
    }
    else
    {
      $this->smarty->assign('cap',$cap);
    	$this->smarty->display('login.html');
    }    
	}
	/**
     * 用户登录数据处理
     *
     */
	public function loginAjax()
	{
		$post = $_POST;
		//根据登录名查询用户信息
		$info = $this->userm->existField('admin',array('user_name' => $post['name']));
		//debug($info);
		//用户是否存在
		if(count($info) > 0)
		{//md5(md5($post['pwd']).$info['pwd_add'])
			//密码是否相同
			if(md5(md5($post['pwd']).$info[0]['pwd_add']) == $info[0]['user_pwd'])
			{
				//验证码是否相同
        $msg = ($post['check'] == $post['capImg']) ? '登录成功' : '验证码错误' ;
        $status = ($post['check'] == $post['capImg']) ? 1 : 0 ;
        if($post['check'] == $post['capImg'])
        {
          //保存用户session
          $data=array('name' => $info[0]['user']);
          $this->session->set_userdata($data);
        }
			}
			else
			{
				$msg = '帐号或密码错误';
				$status = 0;
			}
			
		}
		else
		{
			$msg = '帐号不存在';
			$status = 0;
		}
		echo json_encode(array('msg' => $msg,'status' => $status));
		exit();
	}
  //退出登录
	public function loginOut()
	{
    $this->session->unset_userdata('name');
    //跳转到
    header("Location:". base_url());
	}
  /**
     * 验证码处理函数
     *
     */
  public function captcha()
  {

    $vals = array(
    'word' => rand(100000,999999),
    'img_path' => dirname(BASEPATH).'/static/image/captcha/',
    'img_url' => '/static/image/captcha/',
    'img_width' => 80,
    'img_height' => 30,
    'expiration' => 7200
    );

    $cap = create_captcha($vals);
    return $cap;
  }
  /**
     * 验证码模板处理
     *
     */
  public function capImg()
  {

    $cap = $this->captcha();
    echo json_encode($cap);
    exit();
  }
}
