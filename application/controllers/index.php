<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends My_Controller {

  /**
   * Index Page for this controller.
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct(){
    parent::__construct();
    $this->load->model('orderm');
  }

  public function index()
  {
    $this->smarty->display('index.html');
  }
  public function order()
  {
    $this->smarty->display('order.html');
  }
  /**
     * 预约数据处理
     *
     */
  public function orderAjax()
  {
    $post = $_POST;
    //判断手机号是否使用
    if(count($this->orderm->existField('user_order',array('phone' => $post['phone'])))>0)
    {
      $msg = '手机号已使用';
      echo json_encode(array('msg' => $msg,'status' => 0));
      exit();
    }

    $array = array(
      'name' => $post['name'] ,
      'phone' => $post['phone'] ,
      'type' => $post['type']
      );

    $insertId = $this->orderm->order($array);

    //debug($this->db->last_query());
    $msg = $insertId > 0 ? '预约成功，请等待通知' : '预约失败，请检查信息' ;
    $status = $insertId > 0 ? 1 : 0;
    echo json_encode(array('msg' => $msg,'status' => $status));
    exit();
  }

}

